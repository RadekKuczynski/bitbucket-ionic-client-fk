angular.module('app')

  .factory('ApiService', function($q, $http, $auth) {

    var getUser = function() {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/2.0/user'
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getRepos = function() {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/1.0/user/repositories'
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getRepo = function(owner, slug) {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/2.0/repositories/'+owner+'/'+slug
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getCommits = function(owner, slug, branch) {
      return $http({
        method: 'GET',
        // https://api.bitbucket.org/2.0/repositories/myaccount/coolcode/commits/feature?exclude=master
        url: 'https://api.bitbucket.org/2.0/repositories/'+owner+'/'+slug+'/commits/?include='+branch
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getWatchers = function(owner, slug) {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/2.0/repositories/'+owner+'/'+slug+'/watchers'
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getForks = function(owner, slug) {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/2.0/repositories/'+owner+'/'+slug+'/forks'
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    var getPullrequests = function(owner, slug) {
      return $http({
        method: 'GET',
        url: 'https://api.bitbucket.org/2.0/repositories/'+owner+'/'+slug+'/pullrequests'
      }).then(
        function successCallback(response) {
          console.log("Response:", response);
          return response.status === 200 ? response.data : $q.reject(response);
        });
    };

    return {
      getUser: getUser,
      getRepos: getRepos,
      getRepo: getRepo,
      getCommits, getCommits,
      getWatchers, getWatchers,
      getForks, getForks,
      getPullrequests, getPullrequests
    };
  });
