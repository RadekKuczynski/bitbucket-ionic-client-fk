angular.module('app')

  .controller('RepositoryDetailsCtrl', function($scope, $auth, $stateParams, ApiService) {

    $scope.exposed = {
      "branch": ''
    };

    ApiService.getRepo($stateParams.owner, $stateParams.slug).then(
      function success(data) {
        console.log(data);
        $scope.repo = data;
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );


    $scope.$watch('exposed', function(newv, oldv) {
         console.log(newv.branch);
         ApiService.getCommits($stateParams.owner, $stateParams.slug, newv.branch).then(
           function success(data) {
             $scope.commits = data.values;
             console.log(data);
           },
           function error(response) {
              console.log(response);
              $scope.commits = '';
           }
         );
    }, true);



    ApiService.getWatchers($stateParams.owner, $stateParams.slug).then(
      function success(data) {
        $scope.watchers = data.values.length;
        console.log(data);
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );

    ApiService.getForks($stateParams.owner, $stateParams.slug).then(
      function success(data) {
        $scope.forks = data.values.length;
        console.log(data);
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );

    ApiService.getPullrequests($stateParams.owner, $stateParams.slug).then(
      function success(data) {
        $scope.pullrequests = data.values.length;
        console.log(data);
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );

  });
