angular.module('app')

  .controller('UserCtrl', function($scope, $ionicPopup, ApiService) {
    $scope.data = {};

    ApiService.getUser().then(
      function success(data) {
        $scope.data = data;
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );
  });
